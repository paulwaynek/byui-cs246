package tundidor.src;

import java.util.Scanner;

public class Test {

    private String password;

    public static void main(String[] args) {
        Test ins = new Test();
        Boolean weak = true;
        Boolean valid = false;
        User user;

        while (weak) {
            try {
                // Ask for original password and create user
                System.out.print( "Please type your password: " );
                user = new User(ins.askPassword());

                System.out.print( "Password: " + user.getPassword() + "\n");

                // if is weak it will throw an exception
                NSALoginController.hashUserPassword(user);

                System.out.print( "Salt: " + user.getSalt() + "\n");
                System.out.print( "Hash: " + user.getHashedPassword() + "\n");

                // set weak as false to avid repeat the process
                weak = false;

                //Validate password
                while (! valid) {
                    System.out.print("Please verify your password: ");
                    user.setPassword(ins.askPassword());

                    // verify password
                    valid = NSALoginController.verifyPassword(user);

                    if (! valid) {
                        System.out.print("Invalid Password, please try again\n");
                    }
                }
            } catch ( WeakPasswordException e){
                System.out.print("Weak Password, please remember all passwords must be at least eight characters long " +
                        "and contain at least one digit..\n");
            } catch (Exception e) {
                System.out.print("Oops, something went wrong. Please run it again.\n");
                e.printStackTrace();
            }
        }

        //validation sucess, welcome!
        System.out.print("Welcome to the NSA!");
    }

    private String askPassword(){
        Scanner scanner = new Scanner( System.in );
        String input = scanner.nextLine();

        return input;
    }
}
