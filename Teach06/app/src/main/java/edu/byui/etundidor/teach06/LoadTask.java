package edu.byui.etundidor.teach06;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * Created by etundidor on 2/12/18.
 */

public class LoadTask extends AsyncTask<Void, String, Void> {


    String filename;
    Context context;
    ProgressBar progressBar;
    ArrayAdapter<String> adapter;

    public LoadTask(String pFilename, Context cont, ProgressBar bar, ArrayAdapter<String> eAdapter)
    {
        progressBar = bar;
        context = cont;
        filename = pFilename;
        adapter = eAdapter;
    }

    @Override
    protected Void doInBackground(Void... params) {

        FileInputStream is;
        BufferedReader reader;

        final File file = new File(context.getFilesDir(), filename);

        try {
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            int i = 1;
            while (line != null) {
                Thread.sleep(250);
                publishProgress(line);
                line = reader.readLine();
                i++;
            }

        }catch (Exception e){
            Toast toast = Toast.makeText(context, "Unable to read the file", Toast.LENGTH_SHORT);
            toast.show();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

        progressBar.setProgress(0);

        Toast toast = Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT);
        toast.show();
    }


    protected void onProgressUpdate(String... data) {
        adapter.add(data[0]);
        progressBar.setProgress(adapter.getCount()*10);
    }

    @Override
    protected void onPostExecute(Void result) {

        progressBar.setProgress(100);

        Toast toast = Toast.makeText(context, "File Read Successfully.", Toast.LENGTH_SHORT);
        toast.show();
    }


}
