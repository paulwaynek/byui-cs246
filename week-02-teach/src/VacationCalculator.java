import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VacationCalculator {

    private static int days = 1;

    public static void main(String[] args) {

        VacationCalculator vc = new VacationCalculator();

        System.out.print( "Please type the amout of days you are planning take for vacation: " );
        Scanner scanner = new Scanner( System.in );
        days = Integer.parseInt(scanner.nextLine());


        System.out.format("The total cost for a %d days vacation in Japan is $%2$.2f\n", days, vc.calculateVacationCost(Destination.Japan));
        System.out.format("The total cost for a %d days vacation in Mexico is $%2$.2f\n", days, vc.calculateVacationCost(Destination.Mexico));
        System.out.format("The total cost for a %d days vacation in Europe is $%2$.2f\n", days, vc.calculateVacationCost(Destination.Europe));

    }

    /**
     * Calculates the total cost for vacationing at a given location for
     * a specific number of nights.
     *
     * @param dest the destination of the vacation
     * @return the total cost of the vacation
     */
    public float calculateVacationCost(Destination dest) {
        List<Expense> expenses = new ArrayList<Expense>();

        Cruise cruise = new Cruise(dest);
        expenses.add(cruise);

        Dining dining = new Dining(dest,days);
        expenses.add(dining);

        Lodging lodging = new Lodging(dest,days);
        expenses.add(lodging);

        // you will need it
        Car car = new Car(dest,days);
        expenses.add(car);

        return this.tallyExpenses(expenses);
    }

    /**
     * An internal method used by VacationCalculator to loop through
     * a List of items that implement the Expense interface and
     * determine their cost
     *
     * @param expenses A list of items that implement the Expense interface
     * @return the total cost calculated by the items
     */
    public float tallyExpenses(List<Expense> expenses) {

        float tallyCost = 0;

        for (Expense exp : expenses) {
            tallyCost += exp.getCost();
        }

        return tallyCost;
    }
}