public class Lodging extends ExpensePerDay {

    public Lodging(Destination destination){
        super(destination);
    }

    public Lodging(Destination destination, int nights){
        super(destination,nights);
    }

    protected float getMexicoCost(){
        return 100;
    }

    protected float getEuropeCost(){
        return 200;
    }

    protected float getJapanCost(){
        return 300;
    }

    @Override
    public float getCost() {
        float tax = 1;

        switch (this.destination){
            case Mexico:
                tax = 1;
                break;
            case Europe:
                tax = 1.1f;
                break;
            case Japan:
                tax = 1.3f;
                break;
        }

        return super.getCost() * tax;
    }
}
