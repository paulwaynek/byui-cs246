public class ExpensePerDay extends Expense {

    private int nights = 1;

    public ExpensePerDay (Destination dest){
        this.destination = dest;
    }

    public ExpensePerDay ( Destination dest, int nights ){
        this.destination = dest;
        this.nights = nights;
    }

    public float getCost(){

        cost = super.getCost() * nights;
        return  cost;
    }
}
