public class Dining extends ExpensePerDay {

    public Dining(Destination destination){
        super(destination);
    }

    public Dining(Destination destination, int days){
        super(destination,days);
    }

    protected float getMexicoCost(){
        return 10;
    }

    protected float getEuropeCost(){
        return 20;
    }

    protected float getJapanCost(){
        return 30;
    }
}
