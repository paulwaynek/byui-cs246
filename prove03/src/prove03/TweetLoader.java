package prove03;

import com.google.gson.Gson;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.HashMap;
import java.util.Map;

public class TweetLoader {
    protected Twitter twitter;

    public TweetLoader() {
        ConfigurationBuilder cb = new ConfigurationBuilder();



        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("0qKSsPgQG4I8URZxJfZsgL1qU")
                .setOAuthConsumerSecret("THIS IS SECRET")
                .setOAuthAccessToken("16553060-HjOxjuucLTp0XfuAax6TZ7yf4d9nA3ua45BvM41gH")
                .setOAuthAccessTokenSecret("THIS IS SECRET")
                .setJSONStoreEnabled(true);

        TwitterFactory tf = new TwitterFactory(cb.build());
        this.setTwitter(tf.getInstance());
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    public Map<String,BYUITweet> retrieveTweetsWithHashTag(String hash){
        Map<String,BYUITweet> tweets = new HashMap<String, BYUITweet>();
        Gson gson = new Gson();

        try {
            QueryResult result = this.getTwitter().search(new Query().query(hash));

            for (Status tweet : result.getTweets()) {
                String json = TwitterObjectFactory.getRawJSON(tweet);

                BYUITweet byuiTweet = gson.fromJson(json,BYUITweet.class);
                tweets.put(byuiTweet.getUser().getName(),byuiTweet);
            }
            
        } catch (TwitterException e) {
            e.printStackTrace();
        }

        return tweets;
    }
}
