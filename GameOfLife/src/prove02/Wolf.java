package prove02;

import javax.print.attribute.standard.Destination;
import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class Wolf extends Creature implements Movable,Aggressor,Aware,Spawner {

    Direction preferredDirection;
    Boolean isAbleToSpawn = false;


    public Wolf(){
        preferredDirection = randomDirection();
        _health = 1;
    }

    @Override
    public void attack(Creature target) {
        if (target instanceof Animal) {
            target.takeDamage(5);
            isAbleToSpawn = true;
        }
    }

    @Override
    public void senseNeighbors(Creature above, Creature below, Creature left, Creature right) {

        //Check the current destination
        Creature currentDestination;
        switch (preferredDirection){
            case above:
                currentDestination = above;
                break;
            case below:
                currentDestination = below;
                break;
            case left:
                currentDestination = left;
                break;
            case right:
                currentDestination = right;
                break;
            default:
                currentDestination = above;
        }

        if (currentDestination instanceof Animal) {
            //if the current destination is an Animal then continue in that way
            return;
        }

        if (above instanceof Animal){
            preferredDirection = Direction.above;
        } else if (below instanceof Animal){
            preferredDirection = Direction.below;
        } else if (left instanceof Animal){
            preferredDirection = Direction.left;
        } else if (right instanceof Animal){
            preferredDirection = Direction.right;
        } else {
            preferredDirection = randomDirection();
        }
    }

    protected Direction randomDirection() {
        int randomNum = ThreadLocalRandom.current().nextInt(1, 4);
        Direction direction =  Direction.above;

        switch (randomNum){
            case 1:
                direction = Direction.above;
                break;
            case 2:
                direction = Direction.below;
                break;
            case 3:
                direction = Direction.left;
                break;
            case 4:
                direction = Direction.right;
                break;
        }

        return direction;
    }

    @Override
    public Shape getShape() {
        return Shape.Square;
    }

    @Override
    public Color getColor() {
        return new Color(128,128,128);
    }

    @Override
    public Boolean isAlive() {
        return _health > 0;
    }

    @Override
    public void move() {
        switch (preferredDirection){
            case right:
                _location.x++;
                break;
            case left:
                _location.x--;
                break;
            case above:
                _location.y++;
                break;
            case below:
                _location.y--;
                break;
            default:
                break;
        }
    }


    protected boolean canSpawn() {
        return isAbleToSpawn;
    }

    @Override
    public Creature spawnNewCreature() {
        if (canSpawn()) {
            isAbleToSpawn = false;
            return new Wolf();
        }

        return null;
    }
}
