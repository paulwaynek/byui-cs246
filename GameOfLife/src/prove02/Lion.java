package prove02;

import java.awt.*;

public class Lion extends Animal implements Movable,Aggressor,Spawner {

    Boolean isAbleToSpawn = false;


    Lion(){
        //He is strong
        _health = 100;
    }

    @Override
    public void attack(Creature target) {

        if (target instanceof Creature) {
            // They attack everything
            target.takeDamage(10);
            //but only spawn when attack a plant
            if (target instanceof Plant) {
                isAbleToSpawn = true;
            }
        }
    }

    public Shape getShape() {
        return Shape.RoundRect;
    }

    @Override
    public Color getColor() {
        return new Color(255, 255, 0);
    }

    protected boolean canSpawn() {
        return isAbleToSpawn;
    }

    @Override
    public Creature spawnNewCreature() {
        if (canSpawn()) {
            isAbleToSpawn = false;
            return new Lion();
        }

        return null;
    }

}
